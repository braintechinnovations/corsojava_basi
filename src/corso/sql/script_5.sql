-- Aggiunta di campi note e data iscrizione alla relazione espressa dalla tabella di appoggio
DROP DATABASE IF EXISTS gestione_universitaria;
CREATE DATABASE gestione_universitaria;
USE gestione_universitaria;

CREATE TABLE studente (
	studenteID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(250),
    cognome VARCHAR(250),
    matricola VARCHAR(20) UNIQUE
);

CREATE TABLE esame(
	esameID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(250),
    crediti INTEGER NOT NULL,
    data_esame DATE
);

CREATE TABLE studente_esame(
	studenteRif INTEGER NOT NULL,
    esameRif INTEGER NOT NULL,
    note TEXT,
    data_iscrizione DATETIME DEFAULT NOW(),
    FOREIGN KEY (studenteRif) REFERENCES studente(studenteID) ON DELETE CASCADE,
    FOREIGN KEY (esameRif) REFERENCES esame(esameID) ON DELETE CASCADE,
    UNIQUE(studenteRif, esameRif)
);

INSERT INTO studente (nome, cognome, matricola) VALUES
("Pino", "Pace", "AB123456"),
("Mario", "Rossi", "AB123457"),
("Valeria", "Verdi", "AB123458");

INSERT INTO esame (nome, crediti, data_esame) VALUES
("Analisi I", "6", "2021-06-01"),
("Analisi II", "6", "2021-06-02"),
("Fisica I", "6", "2021-06-03"),
("Fisica II", "6", "2021-06-04"),
("Geometria I", "9", "2021-06-05"),
("Geometria II", "6", "2021-06-06");

INSERT INTO studente_esame (studenteRif, esameRif, note) VALUES
(1,	1, "Esame orale"),
(1,	3, null),
(2,	1, "Laboratorio gi� sostenuto"),
(2,	3, null);

SELECT * 
	FROM studente
    JOIN studente_esame ON studente.studenteID = studente_esame.studenteRif	-- percorso 1
    JOIN esame ON studente_esame.esameRif = esame.esameID;
    
SELECT * 
	FROM esame
    JOIN studente_esame ON esame.esameID = studente_esame.esameRif
    JOIN studente ON studente_esame.studenteRif = studente.studenteID
    WHERE esame.nome = "Analisi I";
    
SELECT studente.matricola, esame.nome, esame.data_esame
	FROM esame
    JOIN studente_esame ON esame.esameID = studente_esame.esameRif
    JOIN studente ON studente_esame.studenteRif = studente.studenteID
    WHERE esame.nome = "Analisi I";
    
SELECT * FROM esame;
DELETE FROM esame WHERE esameID = 1;