DROP DATABASE IF EXISTS carte_fedelta;
CREATE DATABASE carte_fedelta;
USE carte_fedelta;

CREATE TABLE persona (
	personaId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250),
    cognome VARCHAR(250),
    codice_fiscale VARCHAR(16) UNIQUE NOT NULL
);

CREATE TABLE carta_fedelta(
	cartaId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    negozio VARCHAR(250),
    numero_carta VARCHAR(200),
    personaRif INTEGER,
    FOREIGN KEY (personaRif) REFERENCES persona(personaId) ON DELETE SET NULL
);

INSERT INTO persona (nome, cognome, codice_fiscale) VALUES
("Giovanni", "Pace", "PCAGNN"),
("Mario", "Rossi", "MRRRSS"),
("Valeria", "Verdi", "VLRVRD"),
("Marika", "Rossa", "MRKRSS");

SELECT * FROM persona WHERE nome = "Giovanni" AND cognome = "Pace";

INSERT INTO carta_fedelta(negozio, numero_carta, personaRif) VALUES
("Coop", "AB123456", 1),
("Coop", "AB123457", null),
("Coop", "AB123458", 2),
("Coop", "AB123459",  null),
("Conad", "CO123456",  null),
("Conad", "CO123457", 1),
("Conad", "CO123458", 2);

SELECT * FROM persona;
SELECT * FROM carta_fedelta;

-- INNER JOIN - Solo le carte felt� con le relative persone associate
SELECT * 
	FROM carta_fedelta
    INNER JOIN persona ON carta_fedelta.personaRif = persona.personaId;
    
-- LEFT JOIN - Tutte le carte fedelt� con relativa relazione e senza relativa relazione
SELECT * 
	FROM carta_fedelta
    LEFT JOIN persona ON carta_fedelta.personaRif = persona.personaId;
    
-- RIGHT JOIN - Tutti gli utenti che hanno carte fedelt� e non hanno carte fedelt�
SELECT * 
	FROM carta_fedelta
    RIGHT JOIN persona ON carta_fedelta.personaRif = persona.personaId;
    
-- INNER JOIN - Solo le carte felt� relative a Giovanni Pace
SELECT * 
	FROM carta_fedelta
    INNER JOIN persona ON carta_fedelta.personaRif = persona.personaId
    WHERE nome = "Giovanni" AND cognome = "Pace";
    
-- RIGHT JOIN - Mostra tutte le persone a cui non sono associate delle carte.
SELECT * 
	FROM carta_fedelta
    RIGHT JOIN persona ON carta_fedelta.personaRif = persona.personaId;

DELETE FROM persona WHERE personaId = 1;