CREATE DATABASE rubrica_uno;
USE rubrica_uno;

CREATE TABLE persona (
	personaId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250),
    cognome VARCHAR(250),
    codice_fiscale VARCHAR(16) UNIQUE NOT NULL
);

INSERT INTO persona (nome, cognome, codice_fiscale) VALUES 
("Giovanni", "Pace", "PCAGNN"),
("Mario", "Rossi", "MRRRSS"),
("Valeria", "Verdi", "VLRVRD");

INSERT INTO persona (personaId, nome, cognome, codice_fiscale) VALUES 
(45, "Marika", "Rossa", "MRKRSS");

INSERT INTO persona (nome, cognome, codice_fiscale) VALUES 
("Mario", "Rossa", "MRRRSO");

INSERT INTO persona (personaId, nome, cognome, codice_fiscale) VALUES 
(32, "Alessandro", "Rossa", "ALSRSS");

INSERT INTO persona (nome, cognome, codice_fiscale) VALUES 
("Francesco", "Rossa", "FRCRSO");

SELECT * FROM persona;