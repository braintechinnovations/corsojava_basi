CREATE DATABASE rubrica_due;
USE rubrica_due;

CREATE TABLE persona (
	personaId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250),
    cognome VARCHAR(250)
);

INSERT INTO persona (nome, cognome) VALUES 
("Giovanni", "Pace"),
("Mario", "Rossi"),
("Valeria", "Verdi");

INSERT INTO persona (nome, cognome) VALUES 
("Giovanni", "Pace"),
("Giovanni", "Pace");

SELECT * FROM persona;
DELETE FROM persona WHERE personaId = 4;
