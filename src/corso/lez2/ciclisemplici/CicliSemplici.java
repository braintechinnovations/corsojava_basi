package corso.lez2.ciclisemplici;

import java.util.Scanner;

public class CicliSemplici {

	public static void main(String[] args) {

		/*
		 
		 while(condizione){
		 	Blocco di codice
		 }
		 
		 */
		
		int valoreMassimo = 0;			//Ci serve per capire quante volte eseguire l'operazione
		int indiceAttuale = 0;			//Per capire dove siamo!
		
		while(indiceAttuale < valoreMassimo) {
			System.out.println("Valore dell'indice: " + indiceAttuale);
			
			indiceAttuale++;			//indiceAttuale = indiceAttuale + 1;
		}
		
		/*
		 
		 do{
		 	Blocco di codice
		 }while(condizione)
		 
		 */
		
//		int valoreMassimo = 0;
//		int indiceAttuale = 0;
//		
//		do{
//			System.out.println("Valore dell'indice: " + indiceAttuale);
//			indiceAttuale++;
//		}while(indiceAttuale < valoreMassimo);
		
		/*
		 * Costruire un sistema che chieda all'utente per tre volte il nome ed il cognome
		 */
		
//		int valoreMassimo = 3;
//		int indiceAttuale = 0;
//		Scanner interceptor = new Scanner(System.in);
//		
//		while(indiceAttuale < valoreMassimo) {
//			
//			System.out.println("Dimmi il tuo nome:");
//			String nome = interceptor.nextLine();
//			
//			System.out.println("Ciao " + nome);
//			
//			indiceAttuale++;
//			
//		}
//		
//		interceptor.close();
		
//		System.out.println(nome);			//Attenzione, non posso accedere ad una variabile generata nel While
	
		// --------------------------------------------------------------------------- //

//		Scanner interceptor = new Scanner(System.in);
//		boolean scritturaAbilitata = true;
//		
//		while(scritturaAbilitata) {
//			System.out.println("Dimmi qual'� il tuo nome o digita QUIT per uscire!");
//			String input = interceptor.nextLine();
//			
//			if(input.equals("QUIT")) {
//				scritturaAbilitata = false;
//			}
//			else {
//				System.out.println("Ciao " + input);
//			}
//		}
//		
//		interceptor.close();
		
		// --------------------------------------------------------------------------- //
		
		Scanner interceptor = new Scanner(System.in);
		boolean scritturaAbilitata = true;
		
		String elenco = "";
		
		while(scritturaAbilitata) {
			System.out.println("Dimmi qual'� il tuo nome o digita QUIT per uscire!");
			String input = interceptor.nextLine();
			
			if(input.equals("QUIT")) {
				scritturaAbilitata = false;
			}
			else {
				elenco += input + "\n";
			}
		}
		
		System.out.println("ELENCO:");
		System.out.println(elenco);
		
		interceptor.close();
		
		/*
		 * Scrivere un sistema di gestione elenco invitati.
		 * L'inserimento avviene tramite uno scanner che prende in input
		 * (in due tempi diversi) il nome ed il cognome.
		 * 
		 * All'uscita del programma verr� stampato l'elenco delle persone inserite
		 * in precedenza.
		 * 
		 * Prevedere una modifica al codice grazie alla quale l'elenco
		 * delle persone inserite viene stampato ad ogni nuovo inserimento avvenuto
		 * con successo.
		 */
		
		
	}

}
