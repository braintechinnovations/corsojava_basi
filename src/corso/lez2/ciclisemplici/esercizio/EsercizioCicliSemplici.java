package corso.lez2.ciclisemplici.esercizio;

import java.util.Scanner;

public class EsercizioCicliSemplici {

	public static void main(String[] args) {

		/*
		 * Scrivere un sistema di gestione elenco invitati.
		 * L'inserimento avviene tramite uno scanner che prende in input
		 * (in due tempi diversi) il nome ed il cognome.
		 * 
		 * All'uscita del programma verr� stampato l'elenco delle persone inserite
		 * in precedenza.
		 * 
		 * Prevedere una modifica al codice grazie alla quale l'elenco
		 * delle persone inserite viene stampato ad ogni nuovo inserimento avvenuto
		 * con successo.
		 */
		
		Scanner interceptor = new Scanner(System.in);
		
		boolean scritturaAbilitata = true;
		String contenitore = "";
		
		while(scritturaAbilitata) {
			
			System.out.println("Inserisci il nome oppure esci con \"Q\" ");
			String inputNome = interceptor.nextLine();
			
			if(inputNome.equals("Q")) {
				scritturaAbilitata = false;
			}
			else {
				System.out.println("Inserisci il cognome oppure esci con \"Q\" ");
				String inputCognome = interceptor.nextLine();
				
				if(inputCognome.equals("Q")) {
					scritturaAbilitata = false;
				}
				else {					
					String nominativo = inputNome + ", " + inputCognome + "\n";
					contenitore += nominativo;
					
					System.out.println("Elenco degli invitati:");
					System.out.println(contenitore);
				}
			}
			
		}
		
		System.out.println("Elenco degli invitati:");
		System.out.println(contenitore);
		
		
		interceptor.close();
		
	}

}
