package corso.lez2.contenitoricomplessi.esercizio;

public class StatisticheLibreria {

	public static void main(String[] args) {

		String[] libro_1 = {"Storia della buonanotte", 						"Favilli", 	"12345-12345-12345"};
	    String[] libro_2 = {"Nessuno scrive al Federale", 					"Vitali", 	"432434-43423-43243", "SCONTO"};
	    String[] libro_3 = {"Buonvino e il caso del bambino scomparso", 	"Veltroni", "76575-765767-765765"};
	    String[] libro_4 = {"Storia del Buongiorno", 						"Favilli", 	"12345-12345-12645"};
	    String[] libro_5 = {"Il volo del corvo a Roma", 					"Veltroni", "76575-765867-765765"};
	    String[] libro_6 = {"Il volo del gabbiano sul tevere", 				"Veltroni", "76575-765867-765165"};
	    
	    String[][] libreria = { libro_1, libro_2, libro_3, libro_4, libro_5, libro_6 };
	    
	    //Contare tutti i libri di Favilli 
	    
//	    String ricercato = "favilli";
//	    int contatore = 0;
//	    
//	    for(int i=0; i<libreria.length; i++) {
//	    	
//	    	String autTemp = libreria[i][1];
//	    	
//	    	if(autTemp.equalsIgnoreCase(ricercato)) {	//Il ramo else � superfluo quindi lo ignoro
//	    		contatore++;
//	    	}
//	    	
//	    }
//	    
//	    System.out.println(ricercato + " ha scritto: " + contatore + " libri.");
	    
	    // ---------------------------------------------------------------------------- //
	    
	    //Stampare tutti i dettagli del libro con ISBN: 432434-43423-43243
	    
//	    String isbn = "432434-43423-43243";
//	    
//	    for(int i=0; i<libreria.length; i++) {
//	    	
//	    	String isbnTemp = libreria[i][2];
//	    	
//	    	if(isbnTemp.equalsIgnoreCase(isbn)) {
//	    		
//	    		for(int k=0; k<libreria[i].length; k++) {
//	    			System.out.println(libreria[i][k]);
//	    		}
//	    		
////	    		System.out.println(libreria[i][0] + libreria[i][1] + libreria[i][2]);		//Obsoleto
//	    	}
//	    	
//	    }
	    
	    // ----------------------- STAMPA DI TUTTI I DETTAGLI DI OGNI LIBRO ------------------------ //
	    
//	    for(int i=0; i<libreria.length; i++) {				//Scandisce ogni riga!
//	    	
//	    	for(int k=0; k<libreria[i].length; k++) {		//Scandisce ogni colonna DELLA RIGA
//	    		
//	    		System.out.println(libreria[i][k]);
//	    		
//	    	}
//	    	
//	    }
	    
	    
	    // ------------------------------- Limiti di un Array ------------------------------------- //
	    String[] rubrica = new String[3];
	    rubrica[0] = "Giovanni";
	    rubrica[1] = "Mario";
	    rubrica[2] = "Valeria";
	    rubrica[3] = "Mario";		//ERRORE	Out of Bounds
	    rubrica[4] = "Mario";		//ERRORE			
	    
	    
	    
		
	}

}
