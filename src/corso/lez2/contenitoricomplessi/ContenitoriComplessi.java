package corso.lez2.contenitoricomplessi;

public class ContenitoriComplessi {

	public static void main(String[] args) {

		String[][] elenco = {
				{"Giovanni", "Pace", "AB1234"}, 
				{"Mario", "Rossi", "AB1235"}, 
				{"Valeria", "Verdi", "AB1236"} 
			};
	
//		System.out.println(elenco[2][2]);
		
		//Voglio in output tutte le matricole
//		for(int i=0; i<elenco.length; i++) {
//			System.out.println(elenco[i][2]);
//		}
		
//		String ricercata = "AB1235";
//		for(int i=0; i<elenco.length; i++) {
//			
//			if(elenco[i][2].equals(ricercata)) {
//				System.out.println(
//						elenco[i][0] + ", " + 
//						elenco[i][1] + " corrisponde alla matricola " + 
//						elenco[i][2]);
//			}
//			
//		}
		
		// --------------------------------------------------------------- //
		
		String[][] esami = {
				{"Analisi I", "27.5"},
				{"Fisica I", "24.3"}, 
				{"Informatica I", "19.8"}, 
				{"Fisica II", "25.0"}, 
			};

		float somma = 0.0f;
		
		for(int i=0; i<esami.length; i++) {
			
			somma += Float.parseFloat(esami[i][1]);						//somma = somma + ...
			
		}
		
		float media = somma / esami.length;
		
		System.out.println(media);
		
		//System.out.println(esami[2].length);		//Lunghezza di un array contenuto in una cella di memoria
			
//		/*
//		 * Calcolare la media aritmetica di tutti gli esami
//		 */
//		
//		String[][] esamiConPeso = {
//				{"Analisi I", "6", "27.5"},
//				{"Fisica I", "9", "24.3"}, 
//				{"Informatica I", "6", "19.8"}, 
//				{"Fisica II", "9", "25.0"}, 
//			};
//		
//		/*
//		 * CHALLENGE: Calcolare la media ponderata di tutti gli esami
//		 */
	}

}



