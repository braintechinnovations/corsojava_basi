package corso.lez2.inputsemplice;

import java.util.Scanner;

public class InputSemplice {

	public static void main(String[] args) {

//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Ciao, come ti chiami?");
//		String nome = interceptor.nextLine();			//Attende l'input dell'utente che termina con il tasto Invio
//		
//		System.out.println("Ok, ciao " + nome);
		
		/*
		 * Creare un sistema che prenda in input il nome, verifichi per� che questo non sia vuoto.
		 * In caso di nome vuoto restituire un "errore"
		 */
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Dimmi il tuo nome:");
//		String nome = interceptor.nextLine();
//		
//		if(nome.isBlank() || nome.length() < 3) {	//Aggiunta della lunghezza minima
//			System.out.println("Errore :(");
//		}
//		else {
//			System.out.println("Ciao " + nome);
//		}
		
		// ----------- Manipolare Scanner e numeri ----------------- //
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Dimmi quanto sei alto per salire sulla giostra:");
//		float altezza = Float.parseFloat(interceptor.nextLine());				//Cast della Stringa in float
//		
//		if(altezza >= 150) {
//			System.out.println("Puoi entrare");
//		}
//		else {
//			System.out.println("NON Puoi entrare");
//		}
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Dimmi quanti biscotti hai mangiato:");
//		int numeroBiscotti = Integer.parseInt(interceptor.nextLine());			//Cast della Stringa in int
//		
//		if(numeroBiscotti >= 7) {
//			System.out.println("Hai esagerato");
//		}
//		else {
//			System.out.println("NON Hai esagerato");
//		}
		
		/*
		 * Immaginiamo di voler inserire il numero di anni ed il sistema
		 * ci dice se siamo maggiorenni o no!
		 */
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Dimmi quanti anni hai:");
//		int anni = Integer.parseInt(interceptor.nextLine());
//		
//		if(anni <= 0 || anni >= 130) {
//			System.out.println("Errore, fuori dal range");
//		}
//		else {
//			if(anni >= 18) {
//				System.out.println("Maggiorenne");
//			}
//			else {
//				System.out.println("Minorenne");
//			}
//		}
		
		
		/**
		 * TODO:
		 * Verificare la possibilit� di ingresso all'interno di un locale, verificare che:
		 * 
		 *  La temperatura sia compresa tra 35 e 42 gradi
		 *  La temperatura non superi i 37.5 gradi
		 *  Nel caso si possa entrare nel locale, inserire i propri dati (e poi stamparli in console):
		 *  - Nome
		 *  - Cognome
		 *  - Numero di telefono
		 *  
		 *  Nel caso in cui la temperatura non rispetti i limiti imposti sopra, vietare l'ingresso con una frase:
		 *  "ERRORE, non puoi entrare!"
		 */
		
		Scanner interceptor = new Scanner(System.in);
		
		System.out.println("Benvenuto nel nostro risorante.\nInserisci la temperatura:"); // la \n � utilizzata per andare a capo
		float temperatura = Float.parseFloat(interceptor.nextLine());
		
		if(temperatura >= 35.0f && temperatura <= 42.0f) {
			
			if(temperatura <= 37.5f) {
				
				System.out.println("Inserisci il nome:");
				String nome = interceptor.nextLine();
				
				System.out.println("Inserisci il cognome:");
				String cognome = interceptor.nextLine();

				System.out.println("Inserisci il numero di telefono:");
				String telefono = interceptor.nextLine();
				
				String rigaPersona = "Nome: " + nome + "\n";			
				rigaPersona += "Cognome: " + cognome + "\n";			//rigaPersona = rigaPersona + "Cognome: " + cognome + "\n";
				rigaPersona += "Telefono: " + telefono + "\n";
						
				System.out.println(rigaPersona);
			}
			else {
				System.out.println("Errore, scotti!");
			}
				
		}
		else {
			System.out.println("Errore, non puoi entrare!");
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
