package corso.lez1.controlliavanzati.esercizio;

public class RiassegnazioneVariabili {

	public static void main(String[] args) {

		int valore = 5;
		
		valore = valore + 2;		//Il nuovo valore � ugual a quello vecchio + 2
		
		valore = valore - 1;
		
		valore = valore * 2;
		
//		System.out.println(valore);
		
		// ----------------------------------- //
		
		int numero = 19;
		
		numero++;					// numero = numero + 1;
		numero--;					// numero = numero - 1;
		
		System.out.println(numero);
		
	}

}
