package corso.lez1.controlliavanzati.esercizio;

public class Esercizio {

	public static void main(String[] args) {

		/**
		 * CON SWITCH-CASE
		 * Scrivere un piccolo programma che, dato in input il giorno della settimana (in numero), vi restituisca il nome
		 * - 1 = Luned�
		 * - 2 = Marted�
		 * ...
		 * 
		 * NON RISCRIVENDO LO SWITCH-CASE ;)
		 * CHALLENGE... e se volessimo cambiare il sistema di conversione dei giorni nello stile americano?
		 * - 1 = Domenica
		 * - 2 = Luned�
		 * - 3 = Marted�
		 * ...
		 * 
		 * String tipologia = "ITA";
		 * if(tipologia.equals("ENG"))...
		 */
		
		int giorno = 8;
		String tipologia = "ENG";
		
		
		if(giorno >= 1 && giorno <= 7) {
			
			if(tipologia.equals("ENG")) {
				giorno = giorno - 1;
				
				if(giorno == 0) {	//Caso della domenica in Inglese
					giorno = 7;
				}
			}
			
			switch(giorno) {
				case 1:
					System.out.println("Lun");
					break;
				case 2:
					System.out.println("Mar");
					break;
				case 3:
					System.out.println("Mer");
					break;
				case 4:
					System.out.println("Gio");
					break;
				case 5:
					System.out.println("Ven");
					break;
				case 6:
					System.out.println("Sab");
					break;
				case 7:
					System.out.println("Dom");
					break;
//				default:
//					System.out.println("Non riconosciuto");
			}
			
		}
		else {
			System.out.println("Input non compatibile con i valori ammessi!");
		}
		
		
		
	}

}
