package corso.lez1.controllisemplici.esercizio;

public class EsercizioControlli {

	public static void main(String[] args) {

		/*
		 * Creare un sistema di controllo degli ingressi al ristorante:
		 * In input avremo la temperatura (salvata in una variabile).
		 * Se la temperatura � maggiore o uguale di 37.5� vietare l'ingresso al ristorante, se la temperatura
		 * � inferiore a 37.5� permettere l'ingresso.
		 * 
		 * Attenzione!!!!!!!! Non vi vuole un medico per dire che al di sotto dei 34� mostrare un errore,
		 * al di sopra dei 42� mostrare lo stesso errore!
		 */
		
		float temperatura = 32.0f;
		
		if(temperatura <= 34.0f || temperatura >= 42.0f) {
			System.out.println("Presentati come figurante per The walking dead!");
		}
		else {
			if(temperatura >= 37.5f) {
				System.out.println("Non puoi entrare!");
			}
			else {
				System.out.println("Puoi entrare!");
			}
		}
		
		
	}

}
