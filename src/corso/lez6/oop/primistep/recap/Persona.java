package corso.lez6.oop.primistep.recap;

public class Persona {
	
	private String nome;
	private String cognome;
	private int eta;
	private String telefono = "N.D.";
	
	public Persona(){			
		System.out.println("Ho chiamato il costruttore SENZA parametri!");			//"syso" e Ctrl + Spazio
	}
	
	//Persona(String, String)
	public Persona(String varNome, String varCognome) {
		System.out.println("Ho chiamato il costruttore CON 2 parametri!");	
		
		this.nome = varNome;
		this.cognome = varCognome;
	}
	
	//Persona(String, String, int)
	public Persona(String varNome, String varCognome, int varEta) {
		System.out.println("Ho chiamato il costruttore CON 3 parametri!");
		
		this.nome = varNome;
		this.cognome = varCognome;
		this.eta = varEta;
	}
	
	//Persona(int, String, String)
	public Persona(int varEta, String varNome, String varCognome) {
		System.out.println("Ho chiamato il costruttore CON 3 parametri BIS!");			//"syso" e Ctrl + Spazio

		this.nome = varNome;
		this.cognome = varCognome;
		this.eta = varEta;
	}

	public void stampaPersona() {
		System.out.println(this.nome + " " + this.cognome + ", " + this.eta + " " + this.telefono);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	//GETTERS AND SETTERS
	
}
