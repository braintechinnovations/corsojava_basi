package corso.lez6.oop.utilizzo.hw;

public class CodiceFiscale {

	private String codice;
	private String dataScadenza;
	
	public CodiceFiscale() {
		
	}
	
	public CodiceFiscale(String varCodice, String varData) {
		this.codice = varCodice;
		this.dataScadenza = varData;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDataScadenza() {
		return dataScadenza;
	}

	public void setDataScadenza(String dataScadenza) {
		this.dataScadenza = dataScadenza;
	}
	
	
	
}
