package corso.lez6.oop.utilizzo.hw;

public class Persona {

	private String nome;
	private String cognome;
	private CodiceFiscale codFis;
	private CartaIdentita carIde;
	
	public Persona() {
		
	}
	
	public Persona(String varNome, String varCognome) {
		this.nome = varNome;
		this.cognome = varCognome;
	}
	
	public Persona(String varNome,String varCognome,String varCodiceCF,String varDataCF) {
		this.nome = varNome;
		this.cognome = varCognome;
		
		CodiceFiscale codTemporaneo = new CodiceFiscale(varCodiceCF, varDataCF);
		this.codFis = codTemporaneo;
	}
	
	public void associaCartaIdentita(String varCodice, String varDataScade, String varDataEmiss, String varLuogoEmi) {
		CartaIdentita ciTemporanea = new CartaIdentita(varCodice, varDataScade, varDataEmiss, varLuogoEmi);
		this.carIde = ciTemporanea;
	}
	
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public CodiceFiscale getCodFis() {
		return codFis;
	}

	public void setCodFis(CodiceFiscale codFis) {
		this.codFis = codFis;
	}

	public CartaIdentita getCarIde() {
		return carIde;
	}

	public void setCarIde(CartaIdentita carIde) {
		this.carIde = carIde;
	}
	
	
	
}
