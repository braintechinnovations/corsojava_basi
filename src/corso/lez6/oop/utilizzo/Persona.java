package corso.lez6.oop.utilizzo;

public class Persona {

	private String nome;
	private String cognome;
	private int eta;
	private String telefono;
	private Indirizzo spedizione;
	private Indirizzo fatturazione;
	
	public Persona() {
		
	}
	
	public Persona(String varNome, String varCognome) {
		this.nome = varNome;
		this.cognome = varCognome;
	}
	
	public Indirizzo getSpedizione() {
		return this.spedizione;
	}
	public void setSpedizione(Indirizzo objSpedizione) {
		this.spedizione = objSpedizione;
	}
	
	public Indirizzo getFatturazione() {
		return this.fatturazione;
	}
	public void setFatturazione(Indirizzo objFatturazione) {
		this.fatturazione = objFatturazione;
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public void stampaPersona() {
		String risultato = "Nome: " + this.nome + "\n" + 
				"Cognome: " + this.cognome + "\n" + 
				"Et�: " + this.eta + "\n" + 
				"Telefono: " + this.telefono + "\n" +
				"Spedizione: " + this.spedizione.toString() + "\n" +
				"Fatturazione: " + this.fatturazione.toString();
		
		System.out.println(risultato);
	}
	
	public void stampaDettaglio() {
		String risultato = "Nome: " + this.nome + "\n" + 
				"Cognome: " + this.cognome + "\n" + 
				"Et�: " + this.eta + "\n" + 
				"Telefono: " + this.telefono + "\n" +
				"---------------------------------- SPEDIZIONE < \n" +
				"Via: " + this.spedizione.getVia() + "\n" +
				"Civico: " + this.spedizione.getCivico() + "\n" +
				"Citta: " + this.spedizione.getCitta() + "\n" +
				"CAP: " + this.spedizione.getCap() + "\n" +
				"Provincia: " + this.spedizione.getProvincia() + "\n" +
				"---------------------------------- FATTURAZIONE < \n" +
				"Via: " + this.fatturazione.getVia() + "\n" +
				"Civico: " + this.fatturazione.getCivico() + "\n" +
				"Citta: " + this.fatturazione.getCitta() + "\n" +
				"CAP: " + this.fatturazione.getCap() + "\n" +
				"Provincia: " + this.fatturazione.getProvincia() + "\n";
		
		System.out.println(risultato);
	}
	
}
