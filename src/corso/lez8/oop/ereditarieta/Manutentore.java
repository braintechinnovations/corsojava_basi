package corso.lez8.oop.ereditarieta;

public class Manutentore extends Persona{
	
	@Override
	public String toString() {
		return "Manutentore [nome=" + nome + ", cognome=" + cognome + ", telefono=" + telefono + ", email=" + email
				+ "]";
	}

	
	
}
