package corso.lez8.oop.ereditarieta;

public class Persona {

	protected String nome;
	protected String cognome;
	protected String telefono;
	protected String email;
	
	public Persona() {
		
	}

	public Persona(String nome, String cognome, String telefono, String email) {
		this.nome = nome;
		this.cognome = cognome;
		this.telefono = telefono;
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return "Persona [nome=" + nome + ", cognome=" + cognome + ", telefono=" + telefono + ", email=" + email + "]";
	}
	
	
}
