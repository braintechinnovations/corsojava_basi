package corso.lez8.oop.contdinamici.scarpe;

import java.util.ArrayList;
import java.util.List;

public class Negozio {

	private String nomeNegozio;
	private ArrayList<Scarpa> elencoScarpe = new ArrayList<Scarpa>();
	
	public Negozio() {
//		this.elencoScarpe = new ArrayList<Scarpa>();		//Ridondante perch� lo inizializzo direttamtente come valore di default dell'attributo
	}
	
	public Negozio(String nomeNegozio) {
		this.nomeNegozio = nomeNegozio;
//		this.elencoScarpe = new ArrayList<Scarpa>();		//Ridondante perch� lo inizializzo direttamtente come valore di default dell'attributo
	}
	
	public void aggiungiScarpa(Scarpa objScarpa) {
		this.elencoScarpe.add(objScarpa);
	}
	
	public void stampaElenco() {
		System.out.println("--------" + this.nomeNegozio + "--------");
		for(int i=0; i<this.elencoScarpe.size(); i++) {
			Scarpa temp = this.elencoScarpe.get(i);
			System.out.println(temp);
		}
	}
	
	/**
	 * Funzione che mi restituisce il totale delle scarpe presenti nel mio magazzino!
	 * @return
	 */
	public int totaleGiacenza() {
		
		int totale = 0;
		
		for(int i=0; i<this.elencoScarpe.size(); i++) {
			Scarpa temp = this.elencoScarpe.get(i);
			totale = totale + temp.getQuantita();
		}
		
		return totale;
		
	}

	public String getNomeNegozio() {
		return nomeNegozio;
	}

	public void setNomeNegozio(String nomeNegozio) {
		this.nomeNegozio = nomeNegozio;
	}
	
	
}
