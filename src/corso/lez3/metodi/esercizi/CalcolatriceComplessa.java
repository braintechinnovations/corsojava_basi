package corso.lez3.metodi.esercizi;

public class CalcolatriceComplessa {

	//float prima del nome del metodo si chiama "tipo di ritorno"
	public static float somma(float numUno, float numDue) {
		float risultato = numUno + numDue;
		return risultato;
	}
	
	public static void main(String[] args) {

		float sommatoria = somma(16, 9);
		System.out.println(sommatoria);
		
	}

}
