package corso.lez3.metodi;

import java.util.Scanner;

public class MetodiSemplici {
	
	public static void dimmiCiao() {
		System.out.println("CIAO");
	}

	public static void saluta(String nome) {
		System.out.println("Ciao " + nome);
	}
	
	public static void somma(int numUno, int numDue) {
		int somma = numUno + numDue;
		System.out.println("La somma �: " + somma);
	}
	
	public static void verificaTemperatura(float valTemperatura) {
		
		if(valTemperatura <= 32.0f || valTemperatura >= 42.0f) {
			System.out.println("Il valore eccede i limiti umani!");
		}
		else {
			if(valTemperatura >= 37.5f) {
				System.out.println("Non puoi entrare!");
			}
			else {
				System.out.println("Puoi entrare!");
			}
		}
		
	}
	
	public static void main(String[] args) {

//		dimmiCiao();
//		dimmiCiao();
//		dimmiCiao();
		
//		saluta("Giovanni");
//		saluta("Mario");
//		saluta("Valeria");
		
//		somma(15, 8);
//		somma(895,600);
		
//		verificaTemperatura(89.0f);
//		verificaTemperatura(30.0f);
//		verificaTemperatura(37.0f);
//		verificaTemperatura(38.0f);
		
		boolean verificaAbilitata = true;
		Scanner interceptor = new Scanner(System.in);
		
		while(verificaAbilitata) {
			System.out.println("Dimmi la temperatura:");
			String input = interceptor.nextLine();
			
			if(input.equals("Q")) {
				verificaAbilitata = !verificaAbilitata;
			}
			else {
				float inputConvertito = Float.parseFloat(input);
				
				verificaTemperatura(inputConvertito);
			}
		}
		
		/**
		 * Scrivere il codice che mi permetta interattivamente di effettuare operazioni
		 * tra due numeri (con metodi e scanner).
		 * - Somma
		 * - Sottrazione
		 * - Moltiplicazione
		 * - Divisione
		 * 
		 */
		
	}

}
