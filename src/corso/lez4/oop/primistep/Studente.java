package corso.lez4.oop.primistep;

public class Studente {

	private String nome;
	private String cognome;
	private int matricola;
	private String corso;
	
	public Studente() {	//Costruttore senza parametri
		System.out.println("Ho appena creato uno Studente!");
	}
	
	//Costruttore con parametri
	public Studente(String varNome, String varCognome, int varMatricola, String varCorso) {
		this.setNome(varNome);
		this.setCognome(varCognome);
		this.setMatricola(varMatricola);
		this.setCorso(varCorso);
	}
	
	public void setNome(String varNome) {
		this.nome = varNome;
	}
	public void setCognome(String varCognome) {
		this.cognome = varCognome;
	}
	public void setMatricola(int varMatricola) {
		this.matricola = varMatricola;
	}
	public void setCorso(String varCorso) {
		this.corso = varCorso;
	}
	
	public String getNome() {
		return this.nome;
	}
	public String getCognome() {
		return this.cognome;
	}
	public int getMatricola() {
		return this.matricola;
	}
	public String getCorso() {
		return this.corso;
	}
	
	public void stampa() {
		System.out.println(this.nome + "," + this.cognome + "," + this.matricola + "," + this.corso);
	}
	
}
