package corso.lez4.oop.intro;

public class Bottiglia {

	private float altezza;
	private float raggio;
	private String materiale;
	private String colore;
	private String produttore = "Giovanni SRL";	//Valore di default
	
	public void setAltezza(float varAltezza) {
		if(varAltezza >= 0.0f) {
			this.altezza = varAltezza;
		}
		else {
			this.altezza = varAltezza * -1;
		}
	}
	
	public void setRaggio(float varRaggio) {
		if(varRaggio >= 0.0f) {
			this.raggio = varRaggio;
		}
		else {
			this.raggio = varRaggio * -1;
		}
	}
	
	public void setMateriale(String varMateriale) {
		this.materiale = varMateriale;
	}
	public void setColore(String varColore) {
		this.colore = varColore;
	}
	//Non inserisco il metodo SET del produttore per evitarne
	//la modifica
	
	public float getAltezza() {
		return this.altezza;
	}
	public float getRaggio() {
		return this.raggio;
	}
	public String getMateriale() {
		return this.materiale;
	}
	public String getColore() {
		return this.colore;
	}
	public String getProduttore() {
		return this.produttore;
	}
	
	
	public void stampa() {
		System.out.println(
				this.altezza + " " + 
				this.raggio  + " " + 
				this.materiale  + " " + 
				this.colore  + " " + 
				this.produttore);
	}
	
	public float calcolaVolume() {
		float risultato = (3.14f * (this.raggio * this.raggio)) * this.altezza;
		return risultato;
	}
}
