package corso.lez4.recapmetodi;

public class MetodiSempliciRecap_1 {

	/**
	 * Questa funzione si occupa dell'output della stringa Salve....
	 * @param varNome Il nome della persona da salutare
	 */
	public static void saluta(String varNome) {
		System.out.println("Salve " + varNome);
	}
	
	public static void main(String[] args) {

//		System.out.println("Ciao Giovanni");
//		System.out.println("Ciao Mario");
//		System.out.println("Ciao Valeria");	
//		System.out.println("Ciao Marika");	
		
		saluta("Giovanni");
		saluta("Mario");
		saluta("Valeria");
		saluta("Marika");
		
		saluta("Antonio");
	}

}
