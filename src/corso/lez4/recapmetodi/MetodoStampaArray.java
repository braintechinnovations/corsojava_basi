package corso.lez4.recapmetodi;

public class MetodoStampaArray {

	/**
	 * Funzione di stampa dell'elenco numerato dell'array passato al metodo
	 * @param varElenco	Array di stringhe passato al metodo
	 */
	public static void stampaArray(String[] varElenco) {
		for(int i=0; i<varElenco.length; i++) {
			System.out.println((i + 1) + ". " + varElenco[i]);
		}
		System.out.println("-------------------------");
	}
	
	public static void main(String[] args) {

		String[] elencoAuto = {"BMW", "Maserati", "Lamborghini", "Lancia"};
		String[] elencoLinguaggi = {"PHP", "JAVA", "JavaScript", "C", "Fortran"};
		String[] elencoNazioni = {"Italia", "Germania", "Spagna", "Francia", "Grecia"};
		
//		for(int i=0; i<elencoLinguaggi.length; i++) {
//			System.out.println(elencoLinguaggi[i]);
//		}
//		System.out.println("-------------------------");
//		
//		for(int i=0; i<elencoNazioni.length; i++) {
//			System.out.println(elencoNazioni[i]);
//		}
//		System.out.println("-------------------------");
		
		stampaArray(elencoAuto);
		stampaArray(elencoLinguaggi);
		stampaArray(elencoNazioni);
		
		/*
		 * Trasformare tutti gli array di stringhe illustrati sopra in ArrayList e creare un metodo per la stampa.
		 * ArrayList<String> elencoAuto = new ArrayList<String>();
		 * elencoAuto.add....
		 */
	}

}
