package corso.lez4.recapmetodi;

import java.util.ArrayList;

public class MetodoStampaArrayList {
	
	/**
	 * Funzione di stampa dell'elenco sotto forma di ArrayList
	 * @param varElenco
	 */
	public static void stampaArrayList( ArrayList<String> varElenco ) {
		
		for(int i=0; i<varElenco.size(); i++) {
			String temp = varElenco.get(i);
			System.out.println(temp);
		}
	}

	/**
	 * Metodo per la stampa dell'ArrayList con il numero che precede ogni elemento
	 * @param varElenco	ArrayList con l'elenco degli oggetti da stampare
	 */
	public static void stampaArrayListConNumeri( ArrayList<String> varElenco ) {
		
		for(int i=0; i<varElenco.size(); i++) {
			String temp = varElenco.get(i);
			System.out.println( (i+1) + ") " + temp);
		}
		System.out.println("-----------------------------------");
	}
	
	public static void main(String[] args) {
		
		ArrayList<String> elencoAuto = new ArrayList<String>();
		elencoAuto.add("BMW");
		elencoAuto.add("Maserati");
		elencoAuto.add("Lamborghini");
		elencoAuto.add("Lancia");
		
		ArrayList<String> elencoLinguaggi = new ArrayList<String>();
		elencoLinguaggi.add("PHP");
		elencoLinguaggi.add("JAVA");
		elencoLinguaggi.add("JavaScript");
		elencoLinguaggi.add("C");
		elencoLinguaggi.add("Fortran");
		
		ArrayList<String> elencoNazioni = new ArrayList<String>();
		elencoNazioni.add("Italia");
		elencoNazioni.add("Germania");
		elencoNazioni.add("Spagna");
		elencoNazioni.add("Francia");
		elencoNazioni.add("Grecia");
		
//		stampaArrayList(elencoAuto);
//		stampaArrayList(elencoLinguaggi);
//		stampaArrayList(elencoNazioni);
		
		stampaArrayListConNumeri(elencoAuto);
		stampaArrayListConNumeri(elencoLinguaggi);
		stampaArrayListConNumeri(elencoNazioni);
		
	}

}
