package corso.lez4.recapmetodi;

public class MetodiSempliciRecap_2 {

	/**
	 * Funzione di stampa formattata di uno studente
	 * @param varNome		Nome dello studente
	 * @param varMatricola	Matricola dello studente
	 * @param varCorso		Corso dello studente
	 */
	public static void stampaStudenti(String varNome, int varMatricola, String varCorso) {
		String stampa = varNome + ",#" + varMatricola + "," + varCorso;
		System.out.println(stampa);
	}
	
	public static void main(String[] args) {
		
//		System.out.println("Giovanni Pace - #123456 - Ingegneria Informatica");
//		System.out.println("Mario Rossi - #32145 - Ingegneria Meccanica");
//		System.out.println("Valeria Verdi - #98765 - Ingegneria Elettronica");
		
		stampaStudenti("Giovanni Pace", 123456, "Ingegneria Informatica");
		stampaStudenti("Mario Rossi", 32145, "Ingegneria Meccanica");
		stampaStudenti("Valeria Verdi", 98765, "Ingegneria Elettronica");
		
	}
	
}
