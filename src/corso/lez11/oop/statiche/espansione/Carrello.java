package corso.lez11.oop.statiche.espansione;

public class Carrello {

	private float caricoMassimo;
	
	public Carrello(float caricoMassimo) {
		super();
		this.caricoMassimo = caricoMassimo;
		Contatori.incrementaContatore();
	}

	public float getCaricoMassimo() {
		return caricoMassimo;
	}

	public void setCaricoMassimo(float caricoMassimo) {
		this.caricoMassimo = caricoMassimo;
	}
	
	
	
}
