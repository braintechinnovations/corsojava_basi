package corso.lez11.oop.statiche.espansione;

public class Contatori {

	private static int contatore = 0;
	
	/*
	 * Grazie allo static posso evitare di creare l'oggetto Contatori
	 * per accedere al metodo incrementaContatore. Senza lo static infatti
	 * dovrei (per utilizzarlo) prima creare l'oggetto contatore 
	 * Contatori cont = new Contatori();
	 * e poi richiamare il metodo tramite:
	 * cont.incrementaContatori();
	 * 
	 * Grazie allo static posso utilizzarlo con il solo nome della classe:
	 * Contatori.incrementaContatore();
	 */
	public static void incrementaContatore() {
		contatore++;
	}
	
	public static int restituisciContatore() {
		return contatore;
	}
	
}
