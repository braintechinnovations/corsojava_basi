package corso.lez11.oop.statiche.espansione;

public class Bicicletta {

	private String colore;

	public Bicicletta(String colore) {
		super();
		this.colore = colore;
		Contatori.incrementaContatore();
	}

	public String getColore() {
		return colore;
	}

	public void setColore(String colore) {
		this.colore = colore;
	}
	
	
	
}
