package corso.lez11.oop.statiche;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		/*
		 * Funziona ed ottengo zero perch� sto richiamando un metodo statico all'interno di una classe
		 * che non � stata nemmeno istanziata.
		 */
//		System.out.println(Studente.getContatore());
//
//		
//		Studente giovanni 	= new Studente("Giovanni", "Pace", "AB1234");
//		Studente mario 		= new Studente("Mario", "Rossi", "AB1234");
//		Studente valeria 	= new Studente("Valeria", "Verdi", "AB1234");
//
//		System.out.println(Studente.getContatore());
		
		
		Scanner interceptor = new Scanner(System.in);
		
		boolean scritturaAbilitata = true;
		
		while(scritturaAbilitata) {
			
			System.out.println("Effettua una scelta: \n"
					+ "I - Inserimento\n"
					+ "C - Conta\n"
					+ "Q - Uscita\n");
			
			String input = interceptor.nextLine();
			
			switch(input) {
			case "I":
				Studente stud = new Studente();
				
				System.out.println("Nome:");
				stud.setNome(interceptor.nextLine());
				System.out.println("Cognome:");
				stud.setCognome(interceptor.nextLine());
				System.out.println("Matricola:");
				stud.setMatricola(interceptor.nextLine());
				
				System.out.println("Inserimento completato");
				
				break;
			case "C":
				System.out.println("Il numero di studenti inseriti �: " + Studente.getContatore());
				break;
			case "Q":
				scritturaAbilitata = false;
				break;
			}
			
		}
		
	}

}
