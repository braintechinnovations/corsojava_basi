package corso.lez11.oop.polimorfismo.hw;

public class Main {

	public static void main(String[] args) {

		/*
		 * Creare un sistema di gestione menu per uno o pi� ristoranti
		 * Ogni ristorante avr� due categorie di articoli: bevande, piatti
		 * 
		 * Creare dei metodi in grado di:
		 * - Inserire una bevanda o un piatto
		 * - Stampare tutte le bevande o piatti (a scelta)
		 * -> Contare tutte le bevande o piatti o entrambi
		 * 
		 * Ogni articolo avr� ALMENO:
		 * - Codice
		 * - Nome
		 * - Prezzo
		 * 
		 * 1. Fatevi la classe Articolo
		 * 2. Fatevi le classi che estendono Articolo
		 * 3. Negozio che avr� al suo interno un elenco di articoli che sar� effettivamente il menu!
		 */
		
		Ristorante bellaItalia = new Ristorante("Bella Italia");
		bellaItalia.inserisciArticolo("PIATTO", "1234", "Spaghetti alla bolognese", 10.0f, false, 0);
		bellaItalia.inserisciPiatto("123456", "Broccoletti", 5.0f, true);
		bellaItalia.inserisciBevanda("BEV123", "Acqua", 1.5f, 0.5f);
	
		bellaItalia.stampaMenu("BEVANDA");
	}

}
