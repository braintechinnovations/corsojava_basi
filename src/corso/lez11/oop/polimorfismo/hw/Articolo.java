package corso.lez11.oop.polimorfismo.hw;

public abstract class Articolo {

	protected String codice;
	protected String nome;
	protected float prezzo;
	
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	
	public void stampa() {
		String descrizione = "ARTICOLO ---------- \n"
				+ "COD: " + codice + "\n"
				+ "NOM: " + nome + "\n"
				+ "PRZ: " + prezzo + "\n";
		
		System.out.println(descrizione);
	}
	
}
