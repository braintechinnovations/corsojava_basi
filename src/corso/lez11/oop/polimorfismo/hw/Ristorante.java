package corso.lez11.oop.polimorfismo.hw;

import java.util.ArrayList;

public class Ristorante {

	private String nomeRistorante;
	private ArrayList<Articolo> menu = new ArrayList<Articolo>();
	
	/**
	 * Metodo UNIVOCO per creare articoli specificandone il tipo
	 * @param tipo			PIATTO | BEVANDA
	 * @param varCodice
	 * @param varNome
	 * @param varPrezzo
	 * @param varIsVegano
	 * @param varVolume
	 */
	public void inserisciArticolo(
			String tipo, 
			String varCodice, 
			String varNome, 
			float varPrezzo,
			boolean varIsVegano,
			float varVolume) {
		
		switch(tipo) {
			case "PIATTO":
				Piatto tempPiatto = new Piatto(varCodice, varNome, varPrezzo, varIsVegano);
				menu.add(tempPiatto);
				break;
			case "BEVANDA":
				Bevanda tempBevanda = new Bevanda(varCodice, varNome, varPrezzo, varVolume);
				menu.add(tempBevanda);
				break;
		}
	}
	
	/**
	 * Metodo per l'inserimento in menu del SOLO piatto
	 * @param varCodice
	 * @param varNome
	 * @param varPrezzo
	 * @param varIsVegano
	 */
	public void inserisciPiatto(String varCodice, String varNome, float varPrezzo, boolean varIsVegano) {
		Piatto tempPiatto = new Piatto(varCodice, varNome, varPrezzo, varIsVegano);
		menu.add(tempPiatto);
	}
	
	/**
	 * Metodo per l'inserimento in menu della SOLA bevanda
	 * @param varCodice
	 * @param varNome
	 * @param varPrezzo
	 * @param varVolume
	 */
	public void inserisciBevanda(String varCodice, String varNome, float varPrezzo, float varVolume) {
		Bevanda tempBevanda = new Bevanda(varCodice, varNome, varPrezzo, varVolume);
		menu.add(tempBevanda);
	}
	
	public void stampaMenu(String tipo) {
		
		for(int i=0; i<menu.size(); i++) {
			Articolo temp = menu.get(i);		//La traformazione viene effettuata in run-time (binding dinamico)

			if(temp instanceof Piatto && tipo.equals("PIATTO")) {
				Piatto tempPiatto = (Piatto) temp;
				tempPiatto.stampaPiatto();
			}
			
			if(temp instanceof Bevanda && tipo.equals("BEVANDA")) {
				Bevanda tempBevanda = (Bevanda) temp;
				tempBevanda.stampaBibita();
			}
			
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Ristorante(String varNome) {
		this.nomeRistorante = varNome;
	}

	public String getNomeRistorante() {
		return nomeRistorante;
	}

	public void setNomeRistorante(String nomeRistorante) {
		this.nomeRistorante = nomeRistorante;
	}
	
}
