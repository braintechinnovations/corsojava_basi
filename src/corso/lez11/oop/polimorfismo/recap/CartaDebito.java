package corso.lez11.oop.polimorfismo.recap;

public class CartaDebito extends SupportoMagnetico {

	private float deposito = 0;
	private final float limiteGiornalieri = 250;	//Rende IMMUTABILE il valore
	
	public CartaDebito() {
		
	}
	
	public CartaDebito(String varCodice, String varIntestatario, float varDeposito) {
		super.setCodice(varCodice);
		super.setIntestatario(varIntestatario);
		this.deposito = varDeposito;
	}
	
	public float getDeposito() {
		return deposito;
	}
	public void setDeposito(float deposito) {
		this.deposito = deposito;
	}
	public float getLimiteGiornalieri() {
		return limiteGiornalieri;
	}

	public void stampaCartaDebito() {
		String dettaglio = "DETTAGLIO Carta di Debito: \n"
				+ "Intestatario: " + super.getIntestatario() + "\n"
				+ "Codice: " + super.getCodice() + "\n"
				+ "Deposito: " + this.deposito;
		
		System.out.println(dettaglio);
	}
	
}
