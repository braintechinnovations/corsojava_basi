package corso.lez11.oop.polimorfismo.recap;

public class Main {

	public static void main(String[] args) {

		IstitutoBancario unicredit = new IstitutoBancario("Unicredit Centrale", "Via dell'olmo, 23 - Roma");
		
		unicredit.inserisciCarta("CREDITO", "1234 5678 1234 5678", "Giovanni Pace", 3000, 0);
		unicredit.inserisciCarta("CREDITO", "1234 5678 1234 5679", "Mario Rossi", 3000, 0);
		unicredit.inserisciCarta("DEBITO", "1234 5678 1234 5680", "Pino Rossi", 0, 1000);
		
//		unicredit.contaCarte();
		unicredit.stampaTutteLeCarte();
		
		IstitutoBancario bnl = new IstitutoBancario("BNL Centrale", "Via dell'olmo, 24 - Roma");
		bnl.inserisciCarta("CREDITO", "1234 5678 1234 5678", "Giovanni Pace", 3000, 0);
	}

}
