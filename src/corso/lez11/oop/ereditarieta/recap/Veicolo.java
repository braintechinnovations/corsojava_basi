package corso.lez11.oop.ereditarieta.recap;

public abstract class Veicolo {

	protected String proprietario;
	protected String targa;
	protected String telaio;
	
	public String getProprietario() {
		return proprietario;
	}
	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}
	public String getTarga() {
		return targa;
	}
	public void setTarga(String targa) {
		this.targa = targa;
	}
	public String getTelaio() {
		return telaio;
	}
	public void setTelaio(String telaio) {
		this.telaio = telaio;
	}
	
	public String stampaDettaglio() {
		
		String dettaglio = "I dettagli del VEICOLO sono:"
				+ "Proprietario: " + this.proprietario + "\n"
				+ "Targa: " + this.targa + "\n"
				+ "Telaio: " + this.telaio;
		
		return dettaglio;
	}
	
	public abstract void accendi();
	
	
}
