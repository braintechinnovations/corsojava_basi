package corso.lez11.oop.interfacce.recap;

public class Gatto implements Animale{

	private String nome;
	private boolean haGliArtigli = true;
	
	@Override
	public void versoEmesso() {
		System.out.println("Meow");
	}

	@Override
	public void movimento() {
		System.out.println("Cammina");
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
