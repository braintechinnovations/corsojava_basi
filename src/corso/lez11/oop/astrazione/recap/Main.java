package corso.lez11.oop.astrazione.recap;

public class Main {

	public static void main(String[] args) {

//		Animale an = new Animale();	//NONONONO, non posso istanziare un oggetto da una classe astratta!
		
		Gatto edith = new Gatto();
		edith.versoEmesso();
		
		Coccodrillo drillo = new Coccodrillo();
		drillo.versoEmesso();
		
		/*
		 * Metodo di inizializzazione di una classe astratta CHE NECESSITA 
		 * la ridefinizione del metodo astratto versoEmesso
		 */
		Animale fantanimale = new Animale() {
			
			@Override
			public void versoEmesso() {
				System.out.println("CRACRA");
			}
		};
		
		fantanimale.versoEmesso();
	}

}
