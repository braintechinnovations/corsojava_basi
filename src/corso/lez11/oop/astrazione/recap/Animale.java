package corso.lez11.oop.astrazione.recap;

public abstract class Animale {

	protected int numZampe;
	protected boolean vola;
	
	public abstract void versoEmesso();
	
	public int getNumZampe() {
		return numZampe;
	}
	public void setNumZampe(int numZampe) {
		this.numZampe = numZampe;
	}
	public boolean isVola() {
		return vola;
	}
	public void setVola(boolean vola) {
		this.vola = vola;
	}
	
	
	
}
